// import logo from './logo.svg';
import './App.css';
import FunctionalComp from './Components/FunctionalComp';
import ClassComp from './Components/ClassComp';

function App() {
  return (
    <div className="App">
      Hello world!!
      <FunctionalComp></FunctionalComp>
      <ClassComp />
    </div>
  );
}

export default App;
